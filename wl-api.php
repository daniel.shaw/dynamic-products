<?php
/**
 * Plugin Name: Custom API
 * Plugin URL: https://vtcpluginpower.innoenhance.com
 * Description: For custom request or end points.
 * Version: 1.0
 * Author: Daniel Shaw
 * Author URL:
*/
function wl_posts() {
	$args = [
		'numberposts' => 99999,
		'post_type' => 'post'
	];

	$posts = get_posts($args);

	$data = [];
	$i = 0;

	foreach($posts as $post) {
		$data[$i]['id'] = $post->ID;
		$data[$i]['title'] = $post->post_title;
		$data[$i]['content'] = $post->post_content;
		$data[$i]['slug'] = $post->post_name;
		$data[$i]['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
		$data[$i]['featured_image']['medium'] = get_the_post_thumbnail_url($post->ID, 'medium');
		$data[$i]['featured_image']['large'] = get_the_post_thumbnail_url($post->ID, 'large');
		$i++;
	}

	return $data;
}

function wl_post( $slug ) {
	$args = [
		'name' => $slug['slug'],
		'post_type' => 'post'
	];

	$post = get_posts($args);


	$data['id'] = $post[0]->ID;
	$data['title'] = $post[0]->post_title;
	$data['content'] = $post[0]->post_content;
	$data['slug'] = $post[0]->post_name;
	$data['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post[0]->ID, 'thumbnail');
	$data['featured_image']['medium'] = get_the_post_thumbnail_url($post[0]->ID, 'medium');
	$data['featured_image']['large'] = get_the_post_thumbnail_url($post[0]->ID, 'large');

	return $data;
}

function wl_vehicle($params)
{
    $filter_vehicle = $params->get_param('vehicle');
     if($filter_vehicle)
    {
        
        $args = array(
    		    'taxonomy'   => "product_cat",
    		    'orderby'    => 'id',
    		    'order'      => 'ASC',
    		    'hide_empty' =>  false,
    		    'parent' => 0,
    		    
    		);
    	$product_categories = get_terms('product_cat',$args);
    	$data =[];
    	
    	foreach( $product_categories as $key=>$cat ) 
    	{ 
    		$vehicle = wp_get_terms_meta($cat->term_id,'vehicle',true);
    		
    		if(!empty($vehicle) && $vehicle == strtolower($filter_vehicle))
    		{
    		    $data[$key]['category_id'] = $cat->term_id;
    		    $data[$key]['vehicle'] = $vehicle;
    		    $data[$key]['category_name'] = $cat->name;
    		}
    // 		else{
    // 		    $data[$key]['category_id'] = $cat->term_id;
    // 		  //  $data[$key]['vehicle'] = $vehicle;
    // 		    $data[$key]['name'] = $cat->name;
    // 		}
    	}
        return ["success"=> $data, "code"=> 200];
    }
    else{
        return ["error"=> "Please select the vehicale.", "code"=> 400];
    }
}

function wl_brand_types($params){
    $type_id = $params->get_param('type_id');
    $type_id = json_decode($type_id);
    if($type_id){
        $data =[];
        $product_categories = get_terms('product_cat',array('parent' => $type_id, 'hide_empty' => true,'orderby'=> 'id', 'order'=> 'ASC'));
        foreach( $product_categories as $key=>$cat ) 
    	{
    	    $data[$key]['category_id'] = $cat->term_id;
		    $data[$key]['category_name'] = $cat->name;
		    $data[$key]['category_slug'] = $cat->slug;
    	}
		return ["success"=> $data, "code"=> 200];
    }
    else{
        return ["error"=> "Please select the vehicale type.", "code"=> 400];
    }
}

function wl_brand_engine($params){
    $slug = $params->get_param('slug');
    if($slug){
        
        $args = array(
            'post_type' => 'product', 
            'product_cat' => $slug, 
            'posts_per_page' => -1, 
            'orderby' => 'rand', 
            'meta_query' => array(
                array(
                    'key' => '_stock_status',
                    'value' => 'instock',
                    'compare' => '=',
                )
        ));
        $loop = new WP_Query($args);
        
        $data = [];
        $count = 0;
        while ($loop->have_posts()) : $loop->the_post();
            global $post;

            // print_r($post);
            $data[$count]['post_name'] = get_the_title();
            $data[$count]['post_slug'] = $post->post_name;
            $data[$count]['post_id'] = $post->ID;
            $count ++;
        endwhile;
        
        return ["success"=> $data, "code"=> 200];
    }
    else{
        return ["error"=> "Please select the vehicale engine.", "code"=> 400];
    }
}

function wl_products($params){
    $fetch_by_id = $params->get_param('fetch_by_id');
    $fetch_by_id = json_decode($fetch_by_id);
    
    $args = array(
        'post_type'     => 'product_variation',
        'post_status'   => array('private', 'publish'),
        'numberposts'   => -1,
        'orderby'       => 'menu_order',
        'order'         => 'asc',
        'post_parent'   => $fetch_by_id, // get parent post-ID
        
    );
    $variations = get_posts($args);
    $data = [];
    foreach ($variations as $variation) {
		
// 		echo "<pre>";
// 		print_r($variation);
// 		echo "</pre>";
// 		die;
        
        // get variation ID
        $variation_ID = $variation->ID;
    
        // get variations meta
        $product_variation = new WC_Product_Variation($variation_ID);
    
        // get variation featured image
        $variation_image = $product_variation->get_image();
    
        // get variation price
        $variation_price = $product_variation->get_price_html();
        $year_field = get_post_meta($variation_ID, '_year_field', true);
        $fuel_field = get_post_meta($variation_ID, '_fuel_field', true);
        $ori_pow_kw_field = get_post_meta($variation_ID, '_ori_pow_kw_field', true);
        $ori_orig_pow_cv_ps_field = get_post_meta($variation_ID, '_ori_orig_pow_cv/ps_field', true);
        $power_increase_field = get_post_meta($variation_ID, '_power_increase_field', true);
        $torque_increase_field = get_post_meta($variation_ID, '_torque_increase_field', true);
        $tuning_field = get_post_meta($variation_ID, '_tuning_field', true);
    
        $VTC = $product_variation->attributes['vtc'];
        $product_image = "";
        if ($VTC == 'VTC STAGE 1') {
//             $product_image = "https://vtcpluginpower.com/wp-content/uploads/2021/05/NEW-RAPID-STAGE-1-1.png";
			$product_image = "http://vtcpluginpower.com/wp-content/uploads/2021/07/3-3.jpg";
        }
        if ($VTC == 'VTC STAGE 2') {
//             $product_image = "https://vtcpluginpower.com/wp-content/uploads/2021/05/NEW-RAPID-STAGE-2-2.png";
			$product_image = "http://vtcpluginpower.com/wp-content/uploads/2021/07/4-4.jpg";
        }
        if ($VTC == 'VTC PRO') {
			$product_image = "http://vtcpluginpower.com/wp-content/uploads/2021/07/6-6.jpg";
//             $product_image = "https://vtcpluginpower.com/wp-content/uploads/2021/05/RAPID-PRO-2.png";
        }
		
		$productName = explode('-', $variation->post_title);
		
		
		
        $inquiry_link = get_permalink(37)."?product-id=".$variation_ID;
        $data = [
                '_id' => $fetch_by_id,
                'variation_id' => $variation_ID,
                'product_name' => $productName[0],
                'year_field' => $year_field,
                'fuel_field' => $fuel_field,
                'ori_pow_kw_field' => $ori_pow_kw_field,
                'ori_orig_pow_cv_ps_field' => $ori_orig_pow_cv_ps_field,
                'power_increase_field' => $power_increase_field,
                'torque_increase_field' => $torque_increase_field,
                'tuning_field' => $tuning_field,
                'stage' => $VTC,
                'sale_price' => ($product_variation->sale_price)?$product_variation->sale_price:0,
                'add_to_cart_url' => $product_variation->add_to_cart_url(),
                'product_image' => $product_image,
                'inquiry_link' => $inquiry_link,
                
            ];
        // print_r(get_post_meta($variation_ID,'_stock_status',true));
    }
    return ["success"=> $data, "code"=> 200];
}

add_action('rest_api_init', function() {
	register_rest_route('wl/v1', 'posts', [
		'methods' => 'GET',
		'callback' => 'wl_posts',
	]);

	register_rest_route( 'wl/v1', 'posts/(?P<slug>[a-zA-Z0-9-]+)', array(
		'methods' => 'GET',
		'callback' => 'wl_post',
    ) );
	
	register_rest_route('wl/v1', 'vehicle-type', [
		'methods' => 'GET',
		'callback' => 'wl_vehicle',
	]);
	
	register_rest_route('wl/v1', 'brand-types', [
		'methods' => 'GET',
		'callback' => 'wl_brand_types',
	]);
	
	register_rest_route('wl/v1', 'brand-engine', [
		'methods' => 'GET',
		'callback' => 'wl_brand_engine',
	]);
	
	register_rest_route('wl/v1', 'products', [
		'methods' => 'GET',
		'callback' => 'wl_products',
	]);
	
});